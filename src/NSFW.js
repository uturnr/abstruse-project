import React from 'react';

const NSFW = () => {
  return (
    <header className="App-header">
      <p>
        <code><a href="https://www.youtube.com/watch?v=o8mZgs1EJP8">nsfw.</a></code>
      </p>
    </header>
  );
}

export default NSFW;
