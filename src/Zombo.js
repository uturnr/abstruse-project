import React from 'react';

const Zombo = () => {
  return (
    <header className="App-header">
      <p>
        <code><a href="https://html5zombo.com">HTML5</a></code>
      </p>
    </header>
  );
}

export default Zombo;
