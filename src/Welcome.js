import React from 'react';

const Welcome = () => {
  return (
    <header className="App-header">
      <p>
        <code>Nothing to see here.</code>
      </p>
    </header>
  );
}

export default Welcome;
