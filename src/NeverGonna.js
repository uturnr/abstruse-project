import React from 'react';

const NeverGonna = () => {
  return (
    <header className="App-header">
      <p>
        <code><a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ">Never.</a></code>
      </p>
    </header>
  );
}

export default NeverGonna;
