import React from 'react';
import the from './img/1-the.jpg';
import url from './img/2-url.png';
import words from './img/3-words.jpg';
import are from './img/4-are.jpg';
import not from './img/5-not.png';
import relevant from './img/6-relevant.jpg';
import combinationlock from './img/combinationlock.jpg';
import combolock from './img/combolock.jpg';
import lockwithcombination from './img/lockwithcombination.jpg';


import Welcome from './Welcome';

const File = ({ location }) => {
  
  const files = {
    '/1-the': the,
    '/2-url': url,
    '/3-words': words,
    '/4-are': are,
    '/5-not': not,
    '/6-relevant': relevant,
    '/combinationlock': combinationlock,
    '/combolock': combolock,
    '/lockwithcombination': lockwithcombination,
  };
  
  let content = <Welcome />;
  
  if (files[location.pathname]) {
    content = <img alt="" src={files[location.pathname]} />
  }
  
  console.log(location);
  
  return (
    <header className="App-header">
      <p>
        {content}
      </p>
    </header>
  );
}

export default File;
