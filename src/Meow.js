import React from 'react';

const Meow = () => {
  return (
    <header className="App-header">
      <p>
        <code><a href="http://www.emergencykitten.com">Meow?</a></code>
      </p>
    </header>
  );
}

export default Meow;
