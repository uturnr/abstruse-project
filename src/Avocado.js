import React from 'react';

const Avocado = () => {
  return (
    <header className="App-header">
      <p>
        <code><a href="https://www.youtube.com/watch?v=bE4C8a48o1E">Fresh Avocado</a></code>
      </p>
    </header>
  );
}

export default Avocado;
