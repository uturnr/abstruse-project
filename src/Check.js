import React from 'react';

const Check = () => {
  return (
    <header className="App-header">
      <p>
        <code>Check the strategy printer.</code>
      </p>
    </header>
  );
}

export default Check;
