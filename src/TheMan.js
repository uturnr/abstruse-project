import React from 'react';

const TheMan = () => {
  return (
    <header className="App-header">
      <p>
        <code><a href="http://yourethemannowdog.ytmnd.com">The Man</a></code>
      </p>
    </header>
  );
}

export default TheMan;
