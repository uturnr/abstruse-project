import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Welcome from './Welcome';
import NeverGonna from './NeverGonna';
import Zombo from './Zombo';
import NSFW from './NSFW';
import Meow from './Meow';
import Avocado from './Avocado';
import TheMan from './TheMan';
import File from './File';
import Look from './Look';
import Check from './Check';
import './App.css';

function App() {
  return (
    <div className="App">
      <Router>
        <Route exact path="/" component={Welcome} />
        <Route path="/never" component={NeverGonna} />
        <Route path="/html5" component={Zombo} />
        <Route path="/nsfw" component={NSFW} />
        <Route path="/meow" component={Meow} />
        <Route path="/avocado" component={Avocado} />
        <Route path="/theman" component={TheMan} />
        <Route path="/look" component={Look} />
        <Route path="/check" component={Check} />
        <Route component={File} />
      </Router>
    </div>
  );
}

export default App;
