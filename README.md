# Abstruse Project

So abstruse.

Can be cloned without credentials:
`git clone https://gitlab.com/uturnr/abstruse-project.git`

## Setup

`yarn`

## Serve

`sudo PORT=80 yarn run start`

## Routes
### Fun

```
/never
/html5
/meow
/nsfw
/avocado
/theman
```

### Clue

```
/1-the
/2-url
/3-words
/4-are
/5-not
/6-relevant
/combolock
/combinationlock
/lockwithcombination
/look
/check
```